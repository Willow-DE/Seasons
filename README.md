# Seasons
The Willow desktops config and theming tool inspired by Flavours

## Seasons vs Tinty/Flavours
Tinty is like a spiriture sucessor to the now unmaintained Flavours. Tinty themes your desktop applying Base16/24 colors schemes to a your configured files. While this aproach is simple and great for those who need to get things done, Seasons aims to hold your backgrounds, configs, color schemes, and whatever else you tell it too. Have as many diffrent layouts, fonts, etc as you like
